extern crate clap;
use clap::{Arg, App};
use std::process;
use puff;

fn main() {
    let matches = App::new("puff")
                          .version("0.1")
                          .author("Dan D. <ddombrow@gmail.com>")
                          .about("Loading testing a la wrk.")
                          .arg(Arg::with_name("duration")
                               .short("d")
                               .long("duration")
                               .value_name("DURATION")
                               .help("duration of the test, e.g. 2s, 2m, 2h")
                               .takes_value(true))
                          .arg(Arg::with_name("URL")
                               .help("URL to test")
                               .required(true)
                               .index(1))
                          .get_matches();
    
    let duration = matches.value_of("duration").unwrap_or("10s");
    let url = matches.value_of("URL").unwrap();

    println!("duration: {}", duration);
    
    if let Err(e) = puff::run(String::from(url)) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    }
    else {
        println!("Done!");
        process::exit(0);
    }
}
