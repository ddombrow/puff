extern crate actix;
extern crate actix_web;

use std::error::Error;
use futures::future::Future;
use actix_web::client;

pub fn run(url: String) -> Result<(), Box<dyn Error>> {
    println!("url: {}", url);

    // start up the actix system
    let sys = actix::System::new("puff-worker-system");

    let _req1 = client::get("http://www.rust-lang.org")   // <- Create request builder
            .header("User-Agent", "Actix-web")
            .finish().unwrap()
            .send()                               // <- Send http request
            .map_err(|_| ())
            .and_then(|response| {                // <- server http response
                println!("Response: {:?}", response);

                Ok(())
            });
    
    let _req2 = client::get("http://www.google.com")   // <- Create request builder
        .header("User-Agent", "Actix-web")
        .finish().unwrap()
        .send()                               // <- Send http request
        .map_err(|_| ())
        .and_then(|response| {                // <- server http response
            println!("Response: {:?}", response);

            Ok(())
        });

    sys.run();

    Ok(())
}